declare @ParentObjectIntId int, @ObjectIntId int, @ProjectIntId int, @PartitionIntId int, @DataSourceIntId int, @PartitionText varchar(max), @SQLQuery varchar(max)

select @ProjectIntId=ObjectIntId from ProcessObject where ProcessTypeIntId=1 and ObjectId='BICRM'

if @ProjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('BICRM', 'BICRM', 1)
	set @ProjectIntId=@@IDENTITY
end

-- ��������� ������
select @DataSourceIntId=t.ObjectIntId
from ProcessTree t
join ProcessObject o on o.ObjectIntId=t.ObjectIntId
where t.ParentObjectIntId=@ProjectIntId
	and o.ObjectId='DWH'
	and o.ObjectName='DWH'
	and o.ProcessTypeIntId=7


if @DataSourceIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('DWH', 'DWH', 7)
	set @DataSourceIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @DataSourceIntId)
end


-- ���������

set @ObjectIntId=NULL
select @ObjectIntId=t.ObjectIntId
from ProcessTree t
join ProcessObject o on o.ObjectIntId=t.ObjectIntId
where t.ParentObjectIntId=@ProjectIntId
	and o.ObjectId='������ CRM'
	and o.ObjectName='������ CRM'
	and o.ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('������ CRM', '������ CRM', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end
delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWCRMWHTable',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'ClassAdditionalAttrib',0)

-- ���� ���������� CRM
set @ObjectIntId=NULL
select @ObjectIntId=t.ObjectIntId
from ProcessTree t
join ProcessObject o on o.ObjectIntId=t.ObjectIntId
where t.ParentObjectIntId=@ProjectIntId
	and o.ObjectId='���� ���������� CRM'
	and o.ObjectName='���� ���������� CRM'
	and o.ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('���� ���������� CRM', '���� ���������� CRM', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end
delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'External_CRMDespatchType',0)

-- SKU
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ProjectIntId
where o.ObjectId='DWSKU New' 
  and o.ObjectName='SKU' 
  and o.ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('DWSKU New', 'SKU', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWSKUNew',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'ClassAdditionalAttrib',0)

-- ����� ��������
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ProjectIntId
where ObjectId='DW Delivery Address' 
  and ObjectName='����� ��������' 
  and ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('DW Delivery Address', '����� ��������', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_DeliveryAddress',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'ClassAdditionalAttrib',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'Address',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWDeliveryAddress',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'TreeElement',0)

-- ������� � ��
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ProjectIntId
where ObjectId='������� � ���' 
  and ObjectName='������� � ��' 
  and ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('������� � ���', '������� � ��', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'InterCompanyEx',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'Tree',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'TreeElement',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'TreeClass',0)

-- ������ ��������
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ProjectIntId
where ObjectId='������ ��������' 
  and ObjectName='������ ��������' 
  and ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('������ ��������', '������ ��������', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWH_CubeQueryPars',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'Tree',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'TreeElement',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'TreePath',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'WareHouse',0)

-- ��������� ��������
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ProjectIntId
where ObjectId='��������� ��������' 
  and ObjectName='��������� ��������' 
  and ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('��������� ��������', '��������� ��������', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CustNote',0)

-- ����
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ProjectIntId
where ObjectId='v Ware Keg' 
  and ObjectName='����' 
  and ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('v Ware Keg', '����', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'TreeElement',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'TreePath',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'Ware',0)

-- ������ �����
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ProjectIntId
where ObjectId='������ �����' 
  and ObjectName='������ �����' 
  and ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('������ �����', '������ �����', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWPlanVersion',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWPlanType',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWPublishedVersion',0)

--------------------------
-- �������� ����
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ProjectIntId
where ObjectId='View Client Address Net' 
  and ObjectName='�������� ����' 
  and ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('View Client Address Net', '�������� ����', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'ClientAddress',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'ClassAdditionalAttrib',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'CRMCompany',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'TreeElement',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'Tree',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'TreeClass',0)

--------------------------
-- �������� �����
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ProjectIntId
where ObjectId='�������� �����' 
  and ObjectName='�������� �����' 
  and ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('�������� �����', '�������� �����', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'ClientAddress',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'TreeElement',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'Tree',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'ClassAdditionalAttrib',0)

--------------------------
-- ������ ���
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ProjectIntId
where ObjectId='������������ �������' 
  and ObjectName='������������ �������' 
  and ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('������������ �������', '������������ �������', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWREPGroup',0)

--------------------------
-- ��� ������
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ProjectIntId
where ObjectId='��� ������' 
  and ObjectName='��� ������' 
  and ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('��� ������', '��� ������', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWFBType',0)

--------------------------
-- ������ �������
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ProjectIntId
where ObjectId='������ �������' 
  and ObjectName='������ �������' 
  and ProcessTypeIntId=5

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('������ �������', '������ �������', 5)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CRMReceiptLineCargo',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CrmWhBalancePart',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CRMDespatchPart',0)

------------------------------------------------------------------------------------------------------------------------------------------

-- ��� ������������ ������ CRM
set @ParentObjectIntId=NULL
select @ParentObjectIntId=o.ObjectIntId
from ProcessTree t
join ProcessObject o on o.ObjectIntId=t.ObjectIntId
where t.ParentObjectIntId=@ProjectIntId
	and o.ObjectId='������������ ������ CRM'
	and o.ObjectName='������������ ������ CRM'
	and o.ProcessTypeIntId=2

if @ParentObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('������������ ������ CRM', '������������ ������ CRM', 2)
	set @ParentObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ParentObjectIntId)
end

-- ������ ���

-- �������
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ParentObjectIntId
where o.ObjectId='v Ware House Saldo' 
	and o.ObjectName='�������' 
	and o.ProcessTypeIntId=3

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('v Ware House Saldo', '�������', 3)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ParentObjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CRMWhBalance',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CRMDespatch',0)

-- ������
set @PartitionIntId=NULL
select @PartitionIntId=PartitionIntId from ProcessObject where ObjectIntId=@ObjectIntId

-- 2019-05 ������� �������� ����������� DAddressId ��� �������� (dp.DAddressId -> adr.DAddressId)
-- ������ �������� � ������������ ����
set @SQLQuery='<![CDATA[
select
	Despatch = z.QuantityTO,

	CalcBalance = CAST(CASE WHEN (y.PrevQuantity)&lt; 0 THEN 0 ELSE isnull(y.PrevQuantity,0) end AS decimal(16, 3)) 
		+ isnull(z.QuantityTO,0) ,

	Otkl = ABS(CAST(CASE WHEN (y.PrevQuantity)&lt; 0 THEN 0 ELSE isnull(y.PrevQuantity,0) end AS decimal(16, 3)) 
		+ isnull(z.QuantityTO,0)
		- CAST(CASE WHEN (y.Quantity)&lt; 0 THEN 0 ELSE isnull(y.Quantity,0) end AS decimal(16, 3))),
  
	Quantity = CAST(CASE WHEN (y.Quantity)&lt; 0 THEN 0 ELSE isnull(y.Quantity,0) end AS decimal(16, 3)), 

	PrevQuantity = CAST(CASE WHEN (y.PrevQuantity)&lt; 0 THEN 0 ELSE (y.PrevQuantity) end AS decimal(16, 3)), 

	OSG = case 
		when (y.Quantity) &lt;&gt;0 
		then 
			case 
				when CAST(datediff(dd,y.DocumentDate,pd.ExpirationDate)AS decimal(16, 3))/nullif(datediff(dd,pd.ProductionDate,pd.ExpirationDate),0) &lt;0 
				then 0 
				else CAST(datediff(dd,y.DocumentDate,pd.ExpirationDate)AS decimal(16, 3))/nullif(datediff(dd,pd.ProductionDate,pd.ExpirationDate),0) 
			end 
	end,

	OSGPrev = case 
		when (y.PrevQuantity) &lt;&gt;0 
		then 
			case 
				when CAST(datediff(dd,y.DocumentDate,pd.ExpirationDate)AS decimal(16, 3))/nullif(datediff(dd,pd.ProductionDate,pd.ExpirationDate),0) &lt;0 
				then 0 
				else CAST(datediff(dd,y.DocumentDate,pd.ExpirationDate)AS decimal(16, 3))/nullif(datediff(dd,pd.ProductionDate,pd.ExpirationDate),0) 
			end 
	end,
	
	pd.ProductionDate,
	pd.ExpirationDate,
	y.CRMWareHouseId, 
	y.SKUId,
	y.WareId, 
	y.TimeId, 
	y.DAddressId, 
	y.DocumentDate, 
	y.DataPresence,
	 
	FCDAddressId=l.DAddressId,
	a.DAddressSBPCode, 
	a.DAddressDistrCode,

	y.PartNumber, 
	y.CheckTO

-- ������� �� ������ � ����� ���
FROM (
	SELECT  
		CheckTO, 
		sum(Quantity) as Quantity, 
		sum(PrevQuantity) as PrevQuantity,
		CRMWareHouseId, 
		SKUId,
		WareId,
		TimeId,
		DAddressId, 
		DocumentDate, 
		DataPresence,
		PartNumber

	FROM (
		SELECT 
			1 as CheckTO, 
			case when (isnull(cn.quantity,0)) &gt;= (s.Quantity * cuf1.FactorValue / cuf2.FactorValue) then (s.Quantity * cuf1.FactorValue / cuf2.FactorValue) else (isnull(cn.quantity,0)) end AS Quantity, 
			NULL AS PrevQuantity, s.CRMWareHouseId, d.SKUId,s.WareId, t.TimeId, a.DAddressId, s.DocumentDate, 
			DataPresence=1, isnull(s.PartNumber,''��� ������'') as PartNumber
		FROM DW_CRMWhBalancePart AS s 
		INNER JOIN DWTime AS t ON t.TimeFullDate = s.DocumentDate
		INNER JOIN DWSKUNew AS d ON d.SKUCode = s.WareId
		INNER JOIN CrossUnitFactor AS cuf1 ON cuf1.WareId = s.WareId AND cuf1.UnitId = s.UnitId 
		INNER JOIN CrossUnitFactor AS cuf2 ON cuf2.WareId = s.WareId AND cuf2.UnitId = ''dal'' 

		LEFT JOIN DWCRMWareHouseLink AS l ON 
			s.CRMWareHouseId = l.CRMWareHouseId and 
			s.DocumentDate between l.BeginDate and isnull(l.EndDate,''20500101'')

		LEFT JOIN (
			SELECT DAddressId0, MIN(DAddressId) as DAddressId, MIN(DAddressSBPCode) as DAddressSBPCode, MIN(DAddressDistrCode) AS DAddressDistrCode
			FROM DW_DeliveryAddress 
			WHERE DAddressId0 is not null
			GROUP BY DAddressId0
		) AS a ON a.DAddressId0 = l.DAddressId

		outer apply (
			select sum(cn.Quantity) as Quantity from DW_CustNoteDistrParts cn where a.DAddressId=cn.DAddressId and s.WareId=cn.WareId and s.DocumentDate&gt;=cn.MoveDate and s.PartNumber=cn.PartNumber
		) cn
		WHERE 
			s.DocumentDate BETWEEN #MinDate# and dateadd(dd,-1,#MaxDate#)
			and case when (isnull(cn.quantity,0)) &gt;= (s.Quantity * cuf1.FactorValue / cuf2.FactorValue) then (s.Quantity * cuf1.FactorValue / cuf2.FactorValue) else (isnull(cn.quantity,0)) end&gt;0
	
		union all
			SELECT 0 as CheckTO,  
			(s.Quantity * cuf1.FactorValue / cuf2.FactorValue) - (isnull(cn.quantity,0)) AS Quantity, 
			NULL AS PrevQuantity, s.CRMWareHouseId, d.SKUId,s.WareId, t.TimeId, a.DAddressId, s.DocumentDate, 
			DataPresence=1, isnull(s.PartNumber,''��� ������'') as PartNumber
		FROM DW_CRMWhBalancePart AS s 
		INNER JOIN DWTime AS t ON t.TimeFullDate = s.DocumentDate
		INNER JOIN DWSKUNew AS d ON d.SKUCode = s.WareId
		INNER JOIN CrossUnitFactor AS cuf1 ON cuf1.WareId = s.WareId AND cuf1.UnitId = s.UnitId 
		INNER JOIN CrossUnitFactor AS cuf2 ON cuf2.WareId = s.WareId AND cuf2.UnitId = ''dal'' 

		LEFT JOIN DWCRMWareHouseLink AS l ON 
			s.CRMWareHouseId = l.CRMWareHouseId and 
			s.DocumentDate between l.BeginDate and isnull(l.EndDate,''20500101'')

		LEFT JOIN (
			SELECT DAddressId0, MIN(DAddressId) as DAddressId, MIN(DAddressSBPCode) as DAddressSBPCode, MIN(DAddressDistrCode) AS DAddressDistrCode
			FROM DW_DeliveryAddress 
			WHERE DAddressId0 is not null
			GROUP BY DAddressId0
		) AS a ON a.DAddressId0 = l.DAddressId

		outer apply (
			select sum(cn.Quantity) as Quantity from DW_CustNoteDistrParts cn where a.DAddressId=cn.DAddressId and s.WareId=cn.WareId and s.DocumentDate&gt;=cn.MoveDate and s.PartNumber=cn.PartNumber
		) cn
		WHERE 
			s.DocumentDate BETWEEN #MinDate# and dateadd(dd,-1,#MaxDate#)
			and isnull(cn.quantity,0)&lt;(s.Quantity * cuf1.FactorValue / cuf2.FactorValue) 
	
		union all
		SELECT 
			1 as CheckTO, NULL AS Quantity, 
			case when (isnull(cn.quantity,0)) &gt;= (s.Quantity * cuf1.FactorValue / cuf2.FactorValue) then (s.Quantity * cuf1.FactorValue / cuf2.FactorValue) else (isnull(cn.quantity,0)) end AS PrevQuantity, 
			s.CRMWareHouseId, d.SKUId,s.WareId, TimeId=t.TimeId+1, a.DAddressId, DocumentDate=dateadd(day,1,s.DocumentDate), 
			DataPresence=1, isnull(s.PartNumber,''��� ������'') as PartNumber
		FROM DW_CRMWhBalancePart AS s 
		INNER JOIN DWTime AS t ON t.TimeFullDate = s.DocumentDate
		INNER JOIN DWSKUNew AS d ON d.SKUCode = s.WareId
		INNER JOIN CrossUnitFactor AS cuf1 ON cuf1.WareId = s.WareId AND cuf1.UnitId = s.UnitId 
		INNER JOIN CrossUnitFactor AS cuf2 ON cuf2.WareId = s.WareId AND cuf2.UnitId = ''dal'' 
		
		LEFT JOIN DWCRMWareHouseLink AS l ON 
			s.CRMWareHouseId = l.CRMWareHouseId and 
			s.DocumentDate between l.BeginDate and isnull(l.EndDate,''20500101'')

		LEFT JOIN (
			SELECT DAddressId0, MIN(DAddressId) as DAddressId, MIN(DAddressSBPCode) as DAddressSBPCode, MIN(DAddressDistrCode) AS DAddressDistrCode
			FROM DW_DeliveryAddress 
			WHERE DAddressId0 is not null
			GROUP BY DAddressId0
		) AS a ON a.DAddressId0 = l.DAddressId

		outer apply (
			select sum(cn.Quantity) as Quantity from DW_CustNoteDistrParts cn where a.DAddressId=cn.DAddressId and s.WareId=cn.WareId and s.DocumentDate&gt;=cn.MoveDate and s.PartNumber=cn.PartNumber
		) cn
		WHERE 
			s.DocumentDate BETWEEN dateadd(day,-1,#MinDate#) AND dateadd(day,-2,#MaxDate#)
			and case when (isnull(cn.quantity,0)) &gt;= (s.Quantity * cuf1.FactorValue / cuf2.FactorValue) then (s.Quantity * cuf1.FactorValue / cuf2.FactorValue) else (isnull(cn.quantity,0)) end&gt;0

		union all
		SELECT 
			0 as CheckTO, null AS Quantity, 
			(s.Quantity * cuf1.FactorValue / cuf2.FactorValue) - (isnull(cn.quantity,0)) AS PrevQuantity, s.CRMWareHouseId, d.SKUId,s.WareId, TimeId=t.TimeId+1, a.DAddressId, DocumentDate=dateadd(day,1,s.DocumentDate), 
			DataPresence=1, isnull(s.PartNumber,''��� ������'') as PartNumber
		FROM DW_CRMWhBalancePart AS s 
		INNER JOIN DWTime AS t ON t.TimeFullDate = s.DocumentDate
		INNER JOIN DWSKUNew AS d ON d.SKUCode = s.WareId
		INNER JOIN CrossUnitFactor AS cuf1 ON cuf1.WareId = s.WareId AND cuf1.UnitId = s.UnitId 
		INNER JOIN CrossUnitFactor AS cuf2 ON cuf2.WareId = s.WareId AND cuf2.UnitId = ''dal'' 
		
		LEFT JOIN DWCRMWareHouseLink AS l ON 
			s.CRMWareHouseId = l.CRMWareHouseId and 
			s.DocumentDate between l.BeginDate and isnull(l.EndDate,''20500101'')

		LEFT JOIN (
			SELECT DAddressId0, MIN(DAddressId) as DAddressId, MIN(DAddressSBPCode) as DAddressSBPCode, MIN(DAddressDistrCode) AS DAddressDistrCode
			FROM DW_DeliveryAddress 
			WHERE DAddressId0 is not null
			GROUP BY DAddressId0
		) AS a ON a.DAddressId0 = l.DAddressId

		outer apply (
			select sum(cn.Quantity) as Quantity from DW_CustNoteDistrParts cn where a.DAddressId=cn.DAddressId and s.WareId=cn.WareId and s.DocumentDate&gt;=cn.MoveDate and s.PartNumber=cn.PartNumber
		) cn
		WHERE 
			s.DocumentDate BETWEEN dateadd(day,-1,#MinDate#) AND dateadd(day,-2,#MaxDate#)
			and isnull(cn.quantity,0)&lt;(s.Quantity * cuf1.FactorValue / cuf2.FactorValue) 
	) ww
	GROUP BY CheckTO, CRMWareHouseId, SKUId,WareId,TimeId,DAddressId, DocumentDate, DataPresence, PartNumber
) y

LEFT JOIN DW_CRMPartsDim pd on y.PartNumber=pd.PartNumber

LEFT JOIN DWCRMWareHouseLink AS l ON 
	y.CRMWareHouseId = l.CRMWareHouseId and 
	y.DocumentDate between l.BeginDate and isnull(l.EndDate,''20500101'')

LEFT JOIN (
	SELECT 
		DAddressId0, 
		MIN(DAddressId) as DAddressId, 
		MIN(DAddressSBPCode) as DAddressSBPCode, 
		MIN(DAddressDistrCode) AS DAddressDistrCode

	FROM DW_DeliveryAddress 
	WHERE DAddressId0 is not null
	GROUP BY DAddressId0
) AS a ON a.DAddressId0 = l.DAddressId

-- �������� �� �������������	
LEFT JOIN(
	SELECT 
		z.CustDate, 
		z.WareId ,
		z.CRMWareHouseId,
		z.PartNumber,
		z.CheckTO,
		sum(CAST(isnull(e.Direction,1) * isnull(z.QuantityTO,0) AS decimal(16,3)) ) as QuantityTO 
	 	
	FROM DW_CRMDespatchPartChkTO z	
	JOIN External_CRMDespatchType AS e ON e.DocumentTypeId = z.DocumentTypeId 					
	WHERE 
		z.CustDate &gt;= Cast(#MinDate# as smalldatetime) AND
		z.CustDate &lt;  Cast(#MaxDate# as smalldatetime) AND
		e.IsWareHouseMovement = 1
	GROUP BY 
		z.CustDate, 
		z.WareId ,
		z.CRMWareHouseId,
		z.PartNumber,
		z.CheckTO
	HAVING sum(CAST(isnull(e.Direction,1) * isnull(z.QuantityTO,0) AS decimal(16,3)) ) &lt;&gt; 0

) z ON
	z.CustDate = y.DocumentDate AND
	z.WareId = y.WareId AND
	z.CRMWareHouseId = y.CRMWareHouseId AND
	z.PartNumber = y.PartNumber AND 
	z.CheckTO = y.CheckTO
]]>'

if @PartitionIntId IS NULL begin
	insert into ProcessPartition (Period, SQLQuery, DataSourceIntId, MinPartitionDate) values ('quarter',@SQLQuery, @DataSourceIntId, '20180101')

	set @PartitionIntId=@@IDENTITY
	update ProcessObject set PartitionIntId=@PartitionIntId where ObjectIntId=@ObjectIntId
end 
else begin
	update ProcessPartition set SQLQuery=@SQLQuery,
           Period='quarter',
           DataSourceIntId=@DataSourceIntId,
		   MinPartitionDate='20180101'
	where PartitionIntId=@PartitionIntId
end

-- 2019-05 ����� �.
-- ������ 0205-0319 (KPI ���� �� �������)
-- ������� KPI ���� �� �������
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ParentObjectIntId
where o.ObjectId='DDZ_part_wh_balance' 
	and o.ObjectName='��� �� �������' 
	and o.ProcessTypeIntId=3

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('DDZ_part_wh_balance', '��� �� �������', 3)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ParentObjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CRMWhBalance',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CRMDespatch',0)

-- ������
set @PartitionIntId=NULL
select @PartitionIntId=PartitionIntId from ProcessObject where ObjectIntId=@ObjectIntId

set @SQLQuery='<![CDATA[
SELECT
	CASE WHEN PrtChk1.[WareId] IS NULL THEN 0 ELSE 1 END AS [PartChek],	
	CASE WHEN PrtChk2.[WareId] IS NULL THEN 0 ELSE 1 END AS [PartChekWithComp],	
	CASE 
		WHEN 
			ABS(
				isnull(stocks.PrevQuantity, 0) + 
				isnull(mv.Quantity, 0) - 
				isnull(stocks.Quantity, 0)
			) &gt; MDFSC.[MaxDiffForStockChek]
		THEN 0 
		ELSE 1 
	END AS [StockChek],
	CASE 
		WHEN stocks.DocumentDate &gt; stocks.ExpirationDate AND isnull(stocks.Quantity, 0) &gt; 0 AND lwh.isLiquid = 1
		THEN 0 
		ELSE 1 
	END AS [ExpirDateChek], 

	ISNULL(stocks.CRMWareHouseId, mv.CRMWareHouseId) as CRMWareHouseId, 
	d.SKUId, 
	t.TimeId, 
	adr.DAddressId,
	isnull(stocks.PartNumber, isnull(mv.PartNumber, ''��� ������'')) as PartNumber 

FROM(
		SELECT 
			SUM(fct.Quantity) as Quantity,
			SUM(fct.PrevQuantity) as PrevQuantity,
	
			fct.CRMWareHouseId, 
			fct.WareId,  
			fct.DocumentDate,  
			fct.PartNumber,
			convert(datetime, convert(date, fct.[ProductionDate])) as [ProductionDate], --������� ����� �� ������
			convert(datetime, convert(date, fct.[ExpirationDate])) as [ExpirationDate]  --������� ����� �� ������

		-- ������� �� �������������
		FROM(
				-- ������� �� ����� ���			
				SELECT 	
					s.Quantity * cuf1.FactorValue / nullif(cuf2.FactorValue, 0) AS Quantity, 
					0 AS PrevQuantity,
		 
					s.CRMWareHouseId, 
					s.WareId,  
					s.DocumentDate,  

					s.PartNumber,
					s.[ProductionDate],
					s.[ExpirationDate]

				FROM DW_CRMWhBalancePart AS s 
					JOIN CrossUnitFactor AS cuf1 ON 
						cuf1.WareId = s.WareId AND cuf1.UnitId = s.UnitId --����� � ����
					JOIN CrossUnitFactor AS cuf2 ON 
						cuf2.WareId = s.WareId AND cuf2.UnitId = ''dal'' --����� � ����		
				WHERE 
					s.DocumentDate &gt;= Cast(#MinDate# as smalldatetime) AND
					s.DocumentDate &lt;  Cast(#MaxDate# as smalldatetime) AND
					s.Quantity * cuf1.FactorValue / nullif(cuf2.FactorValue, 0) &gt; 0

				UNION ALL

				-- ������� �� ������ ���	
				SELECT 	
					0 AS Quantity, 
					s.Quantity * cuf1.FactorValue / nullif(cuf2.FactorValue, 0) AS PrevQuantity,
		 
					s.CRMWareHouseId, 
					s.WareId,  
					dateadd(day,1, s.DocumentDate) AS DocumentDate, -- ������� �� ������ ��� ��� ������� �� ����� ���������� ��� 

					s.PartNumber,
					s.[ProductionDate],
					s.[ExpirationDate]

				FROM DW_CRMWhBalancePart AS s -- ������� �� ������ �� �������������
					JOIN CrossUnitFactor AS cuf1 ON 
						cuf1.WareId = s.WareId AND cuf1.UnitId = s.UnitId --����� � ����
					JOIN CrossUnitFactor AS cuf2 ON 
						cuf2.WareId = s.WareId AND cuf2.UnitId = ''dal'' --����� � ����		
				WHERE 
					s.DocumentDate &gt;= Cast(#MinDate# as smalldatetime) - 1 AND
					s.DocumentDate &lt;  Cast(#MaxDate# as smalldatetime) - 1 AND
					s.Quantity * cuf1.FactorValue / nullif(cuf2.FactorValue, 0) &gt; 0
		) FCT

		GROUP BY 
			fct.CRMWareHouseId, 
			fct.WareId,  
			fct.DocumentDate, 
		 
			fct.PartNumber,
			convert(datetime, convert(date, fct.[ProductionDate])),
			convert(datetime, convert(date, fct.[ExpirationDate]))
		HAVING 
			SUM(fct.Quantity) &gt; 0 OR
			SUM(fct.PrevQuantity) &gt; 0
) STOCKS

-- �������� �� �������������	
FULL JOIN(
	SELECT 
		dp.CustDate, 
		dp.WareId ,
		d.CRMWareHouseId,
		dp.PartNumber,
		CAST(max(dp.ProductionDate) as date) as ProductionDate,
		CAST(max(dp.ExpirationDate) as date) as ExpirationDate,
		
		SUM(ISNULL(dt.[Direction], 1) * dp.QuantityDal) as Quantity
	 	
	FROM [DW_CRMDespatchPart] dp	
		JOIN [dbo].[DW_CRMDespatch] d ON
			dp.[CustDate]	= d.[CustDate] AND
			dp.[CustNumber] = d.[CustNumber] AND
			dp.[DocumentTypeId] = d.[DocumentTypeId] AND
			dp.[WareId] = d.[WareId] AND
			dp.[CRMDbId] = d.[CRMDbId]	
		JOIN [dbo].[External_CRMDespatchType] AS dt ON
			dp.[DocumentTypeId]	= dt.[DocumentTypeId]					
	WHERE 
		dp.CustDate &gt;= Cast(#MinDate# as smalldatetime) AND
		dp.CustDate &lt;  Cast(#MaxDate# as smalldatetime) AND
		dt.IsWareHouseMovement = 1
	GROUP BY 
		dp.CustDate, 
		dp.WareId ,
		d.CRMWareHouseId,
		dp.PartNumber
	HAVING SUM(ISNULL(dt.[Direction], 1) * dp.QuantityDal) &lt;&gt; 0

) MV ON
	mv.CustDate = stocks.DocumentDate AND
	mv.WareId = stocks.WareId AND
	mv.CRMWareHouseId = stocks.CRMWareHouseId AND
	mv.PartNumber = stocks.PartNumber

JOIN DWTime AS t ON t.TimeFullDate = ISNULL(stocks.DocumentDate, mv.CustDate)

JOIN DWSKUNew AS d ON d.SKUCode = ISNULL(stocks.WareId, mv.WareId) 

LEFT JOIN DWCRMWareHouseLink AS hlink ON 
	hlink.CRMWareHouseId = ISNULL(stocks.CRMWareHouseId, mv.CRMWareHouseId)  and 
	ISNULL(stocks.DocumentDate, mv.CustDate) between hlink.BeginDate and isnull(hlink.EndDate,''20500101'')

LEFT JOIN (
	SELECT DAddressId0, MIN(DAddressId) as DAddressId
	FROM DW_DeliveryAddress 
	WHERE DAddressId0 is not null
	GROUP BY DAddressId0
) AS adr ON adr.DAddressId0 = hlink.DAddressId

LEFT JOIN DW_CRMPartsDim AS part ON part.PartNumber = ISNULL(stocks.PartNumber, mv.PartNumber) 

JOIN ( --������������ ��������� 5 ���
	SELECT 
		ISNULL(CAST(MAX([Value]) AS INT), 5) AS [MaxDiffForStockChek] 
	FROM [dbo].[DWH_CubeQueryPars] 
	WHERE 
		[QueryId] = ''BICRM.DDZByParts'' AND
		[SubTypeId] = ''MaxDiffForStockChek'' AND
		[TypeId] = ''MaxDiffForStockChek''
) AS MDFSC ON 1 = 1

-- ������� ����������� ������� TradeChannel IN (''off-trade'', ''on-trade'')
JOIN( --� LEFT JOIN �������� ���� ��������. ������� ��� ������ ��� ������ ��� �� ���������.
	SELECT       
		a.CRMWarehouseId,
		a.CRMCompanyId,
		CASE 
			WHEN 
				MIN(ISNULL(CH.TradeChannelId, ''off-trade'')) 
				IN 
				(
					SELECT [Value]
					FROM [dbo].[DWH_CubeQueryPars] 
					WHERE 
						[QueryId] = ''BICRM.DDZByParts'' AND
						[SubTypeId] = ''TradeChannelIdForLiquidCRMWarehouse'' AND
						[TypeId] = ''TradeChannelIdForLiquidCRMWarehouse''				
				)
			THEN 1 
			ELSE 0 
		END AS isLiquid
		 
	FROM DWCRMWHTable AS a 
	LEFT JOIN TradeChannelCRMWH AS CH ON a.CRMWarehouseId = CH.CRMWareHouseId
	GROUP BY a.CRMWarehouseId, a.CRMCompanyId

) LWH ON lwh.CRMWarehouseId = ISNULL(stocks.CRMWareHouseId, mv.CRMWareHouseId)

--0871-1119 �������� ������ �� ����������� �����
LEFT JOIN ClassAdditionalAttrib [CAA] ON
	[CAA].Class_Object = ''Ware'' and
	[CAA].AdditionalAttribId = ''BaseSortWareId'' and
	[CAA].ClassId = ISNULL(stocks.[WareId], mv.[WareId])

-- �������� ����������� ������ 1 (��� ��������)
LEFT JOIN (
	SELECT
		[WareId] = CASE WHEN ISNULL([CAAP].Value, '''') = '''' THEN [P].[WareId] ELSE [CAAP].Value END,
		[P].[ProductionDate],
		[P].[ExpirationDate]
	FROM [dbo].[External_CRMReceiptPart] [P]
	LEFT JOIN ClassAdditionalAttrib [CAAP] ON
		[CAAP].Class_Object = ''Ware'' and
		[CAAP].AdditionalAttribId = ''BaseSortWareId'' and
		[CAAP].ClassId = [P].[WareId] 
	WHERE [DocumentIntId] IS NOT NULL
	GROUP BY 
		CASE WHEN ISNULL([CAAP].Value, '''') = '''' THEN [P].[WareId] ELSE [CAAP].Value END,
		[P].[ProductionDate],
		[P].[ExpirationDate]
) as PrtChk1 ON
	PrtChk1.[WareId] = COALESCE([CAA].Value, stocks.[WareId], mv.[WareId]) AND --0871-1119 �������� �� ����������� �����
	PrtChk1.[ProductionDate] = ISNULL(stocks.[ProductionDate], mv.[ProductionDate]) AND
	PrtChk1.[ExpirationDate] = ISNULL(stocks.[ExpirationDate], mv.[ExpirationDate])

-- �������� ����������� ������ 2 (�������� ����������� �����������)
LEFT JOIN (
	SELECT
		[WareId] = CASE WHEN ISNULL([CAAP].Value, '''') = '''' THEN [P].[WareId] ELSE [CAAP].Value END,
		[P].[ProductionDate],
		[P].[ExpirationDate],
		[P].[CompanyId]
	FROM [dbo].[External_CRMReceiptPart] [P]
	LEFT JOIN ClassAdditionalAttrib [CAAP] ON
		[CAAP].Class_Object = ''Ware'' and
		[CAAP].AdditionalAttribId = ''BaseSortWareId'' and
		[CAAP].ClassId = [P].[WareId] 
	WHERE [DocumentIntId] IS NOT NULL
	GROUP BY 
		CASE WHEN ISNULL([CAAP].Value, '''') = '''' THEN [P].[WareId] ELSE [CAAP].Value END,
		[P].[ProductionDate],
		[P].[ExpirationDate],
		[P].[CompanyId]
) as PrtChk2 ON
	PrtChk2.[WareId] = COALESCE([CAA].Value, stocks.[WareId], mv.[WareId]) AND --0871-1119 �������� �� ����������� �����
	PrtChk2.[ProductionDate] = ISNULL(stocks.[ProductionDate], mv.[ProductionDate]) AND
	PrtChk2.[ExpirationDate] = ISNULL(stocks.[ExpirationDate], mv.[ExpirationDate]) AND 
	PrtChk2.[CompanyId] = lwh.CRMCompanyId
]]>'

if @PartitionIntId IS NULL begin
	insert into ProcessPartition (Period, SQLQuery, DataSourceIntId, MinPartitionDate) values ('quarter',@SQLQuery, @DataSourceIntId, '20180101')

	set @PartitionIntId=@@IDENTITY
	update ProcessObject set PartitionIntId=@PartitionIntId where ObjectIntId=@ObjectIntId
end 
else begin
	update ProcessPartition set SQLQuery=@SQLQuery,
           Period='quarter',
           DataSourceIntId=@DataSourceIntId,
		   MinPartitionDate='20180101'
	where PartitionIntId=@PartitionIntId
end
-- 2019-05 ����� �. (�����)

-- ��������
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ParentObjectIntId
where o.ObjectId='v CRM Despatch' 
	and o.ObjectName='��������' 
	and o.ProcessTypeIntId=3

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('v CRM Despatch', '��������', 3)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ParentObjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CRMDespatch',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CustNoteV',0)

-- ������
set @PartitionIntId=NULL
select @PartitionIntId=PartitionIntId from ProcessObject where ObjectIntId=@ObjectIntId

-- 2019-05 ������� �������� ����������� DAddressId (dp.DAddressId -> adr.DAddressId)
set @SQLQuery='<![CDATA[
SELECT        
	dp.AddressRegionType, 
	t.TimeId, 
	s.SKUId, 
	CAST(isnull(e.Direction,1) * dp.QuantityTO AS decimal(16,3)) AS Quantity, 
	CAST(CASE e.Direction WHEN - 1 THEN - dp.QuantityTO ELSE NULL END AS decimal(16, 3)) AS OutQnt, 
	CAST(CASE e.Direction WHEN 1 THEN dp.QuantityTO ELSE NULL END AS decimal(16, 3)) AS InQnt, 
	--dp.DAddressId,
	adr.DAddressId, 
	ISNULL(e.DocumentTypeId, ''Unknown'') AS DocumentTypeId, 
	e.Direction, 
	dp.CRMWareHouseId, 
	dp.clientaddressid,
	dp.CheckTO,
	isnull(dp.PartNumber,''��� ������'') as PartNumber

FROM DW_CRMDespatchPartChkTO AS dp

JOIN DWTime t on dp.CustDate=t.TimeFullDate

JOIN DWSKUNew s on dp.WareId=s.SKUCode

LEFT JOIN DWCRMWareHouseLink AS hlink ON 
	dp.CRMWareHouseId = hlink.CRMWareHouseId  and 
	dp.CustDate between hlink.BeginDate and isnull(hlink.EndDate,''20500101'')

LEFT JOIN (
	SELECT DAddressId0, MIN(DAddressId) as DAddressId
	FROM DW_DeliveryAddress 
	WHERE DAddressId0 is not null
	GROUP BY DAddressId0
) AS adr ON adr.DAddressId0 = hlink.DAddressId

JOIN External_CRMDespatchType AS e ON e.DocumentTypeId = dp.DocumentTypeId 

WHERE 
	(s.SKUId &gt; 0) AND 
	dp.CustDate between #MinDate# and dateadd(dd,-1,#MaxDate#) AND
	e.IsWareHouseMovement = 1

]]>'

if @PartitionIntId IS NULL begin
	insert into ProcessPartition (Period, SQLQuery, DataSourceIntId, MinPartitionDate) values ('quarter',@SQLQuery, @DataSourceIntId, '20180101')

	set @PartitionIntId=@@IDENTITY
	update ProcessObject set PartitionIntId=@PartitionIntId where ObjectIntId=@ObjectIntId
end 
else begin
	update ProcessPartition set SQLQuery=@SQLQuery,
           Period='quarter',
           DataSourceIntId=@DataSourceIntId,
		   MinPartitionDate='20180101'
		where PartitionIntId=@PartitionIntId
end



-- �������� ���
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ParentObjectIntId
where o.ObjectId='v CRM Despatch Kegs' 
	and o.ObjectName='�������� ���' 
	and o.ProcessTypeIntId=3

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('v CRM Despatch Kegs', '�������� ���', 3)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ParentObjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CRMDespatch',0)

-- ������
set @PartitionIntId=NULL
select @PartitionIntId=PartitionIntId 
from ProcessObject where ObjectIntId=@ObjectIntId

if @PartitionIntId IS NULL begin

insert into ProcessPartition (Period, SQLQuery, DataSourceIntId, MinPartitionDate) values ('quarter',
	'<![CDATA[
SELECT 
	d.TimeId, d.WareId, a.DAddressId, ISNULL(e.DocumentTypeId, ''Unknown'') AS DocumentTypeId, e.Direction, d.CRMWareHouseId, 
    CAST(e.Direction * d.Quantity AS decimal(16,3)) AS Quantity, 
    CAST(CASE e.Direction WHEN - 1 THEN - d .Quantity ELSE NULL END AS decimal(16, 3)) AS OutQnt, 
    CAST(CASE e.Direction WHEN 1 THEN d .Quantity ELSE NULL END AS decimal(16, 3)) AS InQnt, d.clientaddressid

FROM DW_CRMDespatch AS d 
LEFT JOIN DWCRMWareHouseLink AS l ON l.CRMWareHouseId = d.CRMWareHouseId and d.CustDate between l.BeginDate and ISNULL(l.EndDate, ''20500101'') 
LEFT JOIN DW_DeliveryAddress AS a ON a.DAddressId0 = l.DAddressId 
JOIN External_CRMDespatchType AS e ON e.DocumentTypeId = d.DocumentTypeId 
JOIN KegsDim AS k ON d.WareId = k.WareId

WHERE 
	d.TimeId between #MinTimeId# and #MaxTimeId#-1 AND
	e.IsWareHouseMovement = 1
	
]]>', @DataSourceIntId, '20090101')

	set @PartitionIntId=@@IDENTITY
	update ProcessObject set PartitionIntId=@PartitionIntId where ObjectIntId=@ObjectIntId
end 
else begin

update ProcessPartition set SQLQuery='<![CDATA[
SELECT 
	d.TimeId, d.WareId, a.DAddressId, ISNULL(e.DocumentTypeId, ''Unknown'') AS DocumentTypeId, e.Direction, d.CRMWareHouseId, 
    CAST(e.Direction * d.Quantity AS decimal(16,3)) AS Quantity, 
    CAST(CASE e.Direction WHEN - 1 THEN - d .Quantity ELSE NULL END AS decimal(16, 3)) AS OutQnt, 
    CAST(CASE e.Direction WHEN 1 THEN d .Quantity ELSE NULL END AS decimal(16, 3)) AS InQnt, d.clientaddressid

FROM DW_CRMDespatch AS d 
LEFT JOIN DWCRMWareHouseLink AS l ON l.CRMWareHouseId = d.CRMWareHouseId and d.CustDate between l.BeginDate and ISNULL(l.EndDate, ''20500101'') 
LEFT JOIN DW_DeliveryAddress AS a ON a.DAddressId0 = l.DAddressId 
JOIN External_CRMDespatchType AS e ON e.DocumentTypeId = d.DocumentTypeId 
JOIN KegsDim AS k ON d.WareId = k.WareId

WHERE 
	d.TimeId between #MinTimeId# and #MaxTimeId#-1 AND
	e.IsWareHouseMovement = 1

]]>',
          Period='quarter',
          DataSourceIntId=@DataSourceIntId,
		  MinPartitionDate='20090101'
	where PartitionIntId=@PartitionIntId
end

-- ������� ���
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ParentObjectIntId
where o.ObjectId='DW CRM Wh Balance Kegs' 
	and o.ObjectName='������� ���' 
	and o.ProcessTypeIntId=3

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('DW CRM Wh Balance Kegs', '������� ���', 3)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ParentObjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CRMWhBalance',0)


-- ������� �� � �����
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ParentObjectIntId
where o.ObjectId='DW CRM Wh Balance SK Uin Kegs' 
	and o.ObjectName='������� �� � �����' 
	and o.ProcessTypeIntId=3

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('DW CRM Wh Balance SK Uin Kegs', '������� �� � �����', 3)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ParentObjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CRMWhBalance',0)

-- ������
set @PartitionIntId=NULL
select @PartitionIntId=PartitionIntId 
from ProcessObject where ObjectIntId=@ObjectIntId

if @PartitionIntId IS NULL begin

	insert into ProcessPartition (Period, SQLQuery, DataSourceIntId,MinPartitionDate) values ('year',
	'<![CDATA[SELECT DAddressId, TimeId, SKUId, WareId, CRMWareHouseId, DocumentDate, Quantity, PrevQuantity
FROM DW_CRMWhBalanceSKUinKegs
where TimeId between #MinTimeId# and #MaxTimeId#-1]]>', @DataSourceIntId, '20090101')

	set @PartitionIntId=@@IDENTITY
	update ProcessObject set PartitionIntId=@PartitionIntId where ObjectIntId=@ObjectIntId
end 
else begin

	update ProcessPartition set SQLQuery='<![CDATA[SELECT DAddressId, TimeId, SKUId, WareId, CRMWareHouseId, DocumentDate, Quantity, PrevQuantity
FROM DW_CRMWhBalanceSKUinKegs
where TimeId between #MinTimeId# and #MaxTimeId#-1]]>',
          Period='year',
          DataSourceIntId=@DataSourceIntId,
		  MinPartitionDate='20090101'
	where PartitionIntId=@PartitionIntId
end

-- ��������
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId 
from ProcessObject o
join ProcessTree t on o.ObjectIntId=t.ObjectIntId
where o.ObjectId='v Cust Note' 
	and o.ObjectName='��������' 
	and o.ProcessTypeIntId=3
	and t.ParentObjectIntId=@ParentObjectIntId

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('v Cust Note', '��������', 3)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ParentObjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CustNoteV',0)

-- ������
set @PartitionIntId=NULL

select @PartitionIntId=PartitionIntId 
from ProcessObject where ObjectIntId=@ObjectIntId

set @PartitionText='<![CDATA[
SELECT 
  s.SKUId, t.TimeId, ic.InterCompanyIntId, 
		ISNULL(a.DAddressId, 0) AS DAddressId,  cn.DocumentIntId, cn.DocumentNumber, cn.WareHouseId, cn.MoveDate
		,(al.Quantity * cuf1.FactorValue / cuf2.FactorValue)  as  DespatchDal , al.ActionDate as ProductionDate, isnull(pd.PartNumber,''��� ������'') as PartNumber,
	OSG =
	case when (al.Quantity) &lt;&gt;0 
	then 
		case when CAST(datediff(dd,cn.MoveDate,pd.ExpirationDate)AS decimal(16, 3))/nullif(datediff(dd,pd.ProductionDate,pd.ExpirationDate),0) &lt;0 
		then 0 
		else CAST(datediff(dd,cn.MoveDate,pd.ExpirationDate)AS decimal(16, 3))/nullif(datediff(dd,pd.ProductionDate,pd.ExpirationDate),0) 
		end 
	end,
		OSGn =
	case when (al.Quantity) &lt;&gt;0 and case when CAST(datediff(dd,cn.MoveDate,pd.ExpirationDate)AS decimal(16, 3))/nullif(datediff(dd,pd.ProductionDate,pd.ExpirationDate),0) &lt;0 
		then 0 
		else CAST(datediff(dd,cn.MoveDate,pd.ExpirationDate)AS decimal(16, 3))/nullif(datediff(dd,pd.ProductionDate,pd.ExpirationDate),0) 
		end &gt;=0.7 then 0 else 1 end,
	pd.ExpirationDate
	FROM	DW_CustNoteV AS cn
		left JOIN DWTime AS t ON t.TimeFullDate = cn.MoveDate
		left JOIN InterCompany AS ic ON ic.InterCompanyId = cn.InterCompanyId
		left JOIN DW_DeliveryAddress AS a ON a.DAddressCode = cn.DeliveryAddressId
		left JOIN Ware AS w ON w.WareId = cn.WareId
		left JOIN DWSKUNew AS s ON s.SKUCode = ISNULL(w.PriceWareId, w.WareId)

		left join WareComponent wc  
			join TreeRef tr on wc.ComponentId=tr.ElementId and tr.TreeClassId=''WareTree'' and tr.TreeClassId=''WareTree'' and tr.TreeId=''0001''
			on cn.WareId=wc.WareId
		left join DW_CustNoteLine cnl on cn.DocumentIntId=cnl.DocumentIntId and wc.ComponentId=cnl.WareId and cn.SourceDBId=cnl.SourceDBId and cn.LineNumber=cnl.ParentLineNumber and cnl.LineType = ''S''  
		left join AnalytLotLink al on al.DocumentIntId=cn.DocumentIntId and al.LineNumber=cnl.LineNumber and al.Action = ''S''
		left join AnalytLot an on al.AnalytLotIntId=an.AnalytLotIntId
		left join Ware wl on an.WareId=wl.WareId
		left JOIN CrossUnitFactor AS cuf1 ON cuf1.WareId = an.WareId AND cuf1.UnitId = wl.BaseUnitId 
		left JOIN CrossUnitFactor AS cuf2 ON cuf2.WareId = an.WareId AND cuf2.UnitId = ''dal''
		left join DW_CRMPartsDim pd on pd.WareId=s.SKUCode and pd.ProductionDate=CONVERT(varchar(8), an.ActionDate , 112) and pd.PartGroup=''���������� ������''

	WHERE cn.WareHouseTreeId &lt;&gt; ''00.0001Tr'' and cn.MoveDate between #MinDate# and Cast(#MaxDate# as smalldatetime)-1
]]>'

if @PartitionIntId IS NULL begin
	insert into ProcessPartition (Period, SQLQuery, DataSourceIntId, MinPartitionDate) values ('year', @PartitionText, @DataSourceIntId, '20180101')

	set @PartitionIntId=@@IDENTITY
	update ProcessObject set PartitionIntId=@PartitionIntId where ObjectIntId=@ObjectIntId
end 
else begin
	update ProcessPartition set SQLQuery=@PartitionText, Period='year', DataSourceIntId=@DataSourceIntId, MinPartitionDate='20180101'
	where PartitionIntId=@PartitionIntId
end


-- �������
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId 
from ProcessObject o
join ProcessTree t on o.ObjectIntId=t.ObjectIntId
where o.ObjectId='v DW Cust Note En Route' 
	and o.ObjectName='�������' 
	and o.ProcessTypeIntId=3
	and t.ParentObjectIntId=@ParentObjectIntId

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('v DW Cust Note En Route', '�������', 3)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ParentObjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CustNoteEnRoute',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DA_WBConfirmDate',0)

-- ������
set @PartitionIntId=NULL

select @PartitionIntId=PartitionIntId 
from ProcessObject where ObjectIntId=@ObjectIntId

set @PartitionText='<![CDATA[
SELECT
 cn.DocumentIntId, s.SKUId, ISNULL(a.DAddressId, 0) AS DAddressId, a.DAddressId0, a.DAddressSBPCode, ic.InterCompanyIntId,  
   cn.ActionDate, cn.MoveDate, cn.DAWBConfirmDate, cn.WBConfirmDate, t.TimeId, tn.TimeId AS MoveTimeId,
   sum(al.Quantity * cuf1.FactorValue / cuf2.FactorValue) as Quantity , al.ActionDate as ProductionDate, isnull(pd.PartNumber,''��� ������'') as PartNumber
  FROM DW_CustNoteEnRoute AS cn LEFT JOIN
    Ware AS w ON w.WareId = cn.WareId LEFT JOIN
    DWSKUNew AS s ON s.SKUCode = ISNULL(w.PriceWareId, w.WareId) LEFT OUTER JOIN
    DW_DeliveryAddress AS a ON cn.DeliveryAddressId = a.DAddressCode LEFT OUTER JOIN
    DW_InterCompany AS ic ON cn.InterCompanyId = ic.InterCompanyId 
    left JOIN DWTime AS t ON t.TimeFullDate BETWEEN cn.MoveDate AND cn.WBConfirmDate 
    left JOIN DWTime AS tn ON tn.TimeFullDate = cn.MoveDate
    left join WareComponent wc on cn.WareId=wc.WareId
    left join DW_CustNoteLine cnl on cn.DocumentIntId=cnl.DocumentIntId and wc.ComponentId=cnl.WareId
    left join AnalytLotLink al on al.DocumentIntId=cn.DocumentIntId and al.LineNumber=cnl.LineNumber
    left join AnalytLot an on al.AnalytLotIntId=an.AnalytLotIntId
    left join Ware wl on an.WareId=wl.WareId
    left JOIN CrossUnitFactor AS cuf1 ON cuf1.WareId = an.WareId AND cuf1.UnitId = wl.BaseUnitId 
    left JOIN CrossUnitFactor AS cuf2 ON cuf2.WareId = an.WareId AND cuf2.UnitId = ''dal''
    left join DW_CRMPartsDim pd on pd.WareId=cn.WareId and pd.ProductionDate=al.ActionDate 
  WHERE cn.WBConfirmDate between #MinDate# and Cast(#MaxDate# as smalldatetime)-1
    and cn.MoveDate&gt;=''20180101''
    and not exists(select Value from DWH_CubeQueryPars where QueryId=''vCustNoteTransit'' and TypeId=''ExportExclude'' and SubTypeId=''SalesDirCode'' and Value=isnull(a.DAddressDirCode,''''))
  GROUP BY cn.DocumentIntId, s.SKUId, a.DAddressId, a.DAddressId0, a.DAddressSBPCode, ic.InterCompanyIntId, cn.CompanyId, cn.ActionDate, cn.MoveDate, 
   cn.DAWBConfirmDate, cn.WBConfirmDate, 
     a.DAddressDirCode, a.DAddressContractTypeCode, t.TimeId, tn.TimeId,al.ActionDate,pd.PartNumber
]]>'

if @PartitionIntId IS NULL begin
	insert into ProcessPartition (Period, SQLQuery, DataSourceIntId, MinPartitionDate) values ('year', @PartitionText, @DataSourceIntId, '20180101')

	set @PartitionIntId=@@IDENTITY
	update ProcessObject set PartitionIntId=@PartitionIntId where ObjectIntId=@ObjectIntId
end 
else begin
	update ProcessPartition set SQLQuery=@PartitionText, Period='year', DataSourceIntId=@DataSourceIntId, MinPartitionDate='20180101'
	where PartitionIntId=@PartitionIntId
end


-- �� ������ ��� ���
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ParentObjectIntId
where ObjectId='DWREP Group Rels DA' 
	and ObjectName='������ ��� ���' 
	and ProcessTypeIntId=3

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('DWREP Group Rels DA', '������ ��� ���', 3)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ParentObjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWREPGroupRels',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWREPGroup',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'TradeRegionEx',0)

-- �� ��� ������ ���
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ParentObjectIntId
where ObjectId='DWFB Group Rels DA' 
	and ObjectName='��� ������ ���' 
	and ProcessTypeIntId=3

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('DWFB Group Rels DA', '��� ������ ���', 3)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ParentObjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWREPGroupRels',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWREPGroup',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWFocusBrandGroup',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWFBGroupRels',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DWSKUNew',0)



/*
-- ��� �������� ���������������
set @ParentObjectIntId=NULL
select @ParentObjectIntId=o.ObjectIntId
from ProcessTree t
join ProcessObject o on o.ObjectIntId=t.ObjectIntId
where t.ParentObjectIntId=@ProjectIntId
	and o.ObjectId='�������� ���������������'
	and o.ObjectName='�������� ���������������'
	and o.ProcessTypeIntId=2

if @ParentObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('�������� ���������������', '�������� ���������������', 2)
	set @ParentObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ProjectIntId, @ParentObjectIntId)
end




-- ������ ���


-- �� ��������� ������� � ������������ SKU ------------------------------------------
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ParentObjectIntId
where ObjectId='v DW CRM Despatch Sum Generalized' 
	and ObjectName='��������� ������� � ������������ SKU' 
	and ProcessTypeIntId=3

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('v DW CRM Despatch Sum Generalized', '��������� ������� � ������������ SKU', 3)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ParentObjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CRMDespatchSumGeneralizedMultiPack',0)
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CRMWhBalance',0)


---- �������
set @PartitionIntId=NULL
select @PartitionIntId=PartitionIntId 
from ProcessObject where ObjectIntId=@ObjectIntId

set @SQLQuery = '<![CDATA[    
SELECT x.DAddressId, x.QuantityDal, x.QuantitySum, x.GeneralizedSKUId, x.TimeId,   
	CASE WHEN isnull(x.QuantitySum,0) - isnull(y.Quantity,0) &gt; 0 THEN isnull(x.QuantitySum,0) - isnull(y.Quantity,0) ELSE NULL END AS OOS 
FROM DW_CRMDespatchSumGeneralizedMultiPack AS x  
LEFT OUTER JOIN (  
	SELECT SUM(b.Quantity * cuf1.FactorValue / cuf2.FactorValue) AS Quantity, a.DAddressId, t.TimeId, s.GeneralizedSKUId  
	FROM DW_CRMWhBalance AS b   
	INNER JOIN DWCRMWareHouseLink AS l ON l.CRMWareHouseId = b.CRMWareHouseId and b.DocumentDate between l.BeginDate and ISNULL(l.EndDate, ''20500101'')
	INNER JOIN DW_DeliveryAddress AS a ON l.DAddressId = a.DAddressId0   
	INNER JOIN DWTime AS t ON t.TimeFullDate = b.DocumentDate   
	INNER JOIN DWSKUGeneralizedMultiPack AS s ON s.SKUCode = b.WareId   
	INNER JOIN CrossUnitFactor AS cuf1 ON cuf1.WareId = b.WareId AND cuf1.UnitId = b.UnitId   
	INNER JOIN CrossUnitFactor AS cuf2 ON cuf2.WareId = b.WareId AND cuf2.UnitId = ''dal'' 
	where b.DocumentDate between dateadd(dd,-1,#MinDate#) and dateadd(dd,-2,#MaxDate#) 
	GROUP BY a.DAddressId, t.TimeId, s.GeneralizedSKUId 
) AS y ON y.TimeId = x.TimeId - 1 AND y.DAddressId = x.DAddressId AND y.GeneralizedSKUId = x.GeneralizedSKUId where  x.TimeId between #MinTimeId# and #MaxTimeId#-1
 ]]>'

if @PartitionIntId IS NULL begin
	insert into ProcessPartition (Period, SQLQuery, DataSourceIntId) values ('quarter', @SQLQuery, @DataSourceIntId)

	set @PartitionIntId=@@IDENTITY
	update ProcessObject set PartitionIntId=@PartitionIntId where ObjectIntId=@ObjectIntId
end 
else begin
	update ProcessPartition set SQLQuery=@SQLQuery,
           Period='quarter',
           DataSourceIntId=@DataSourceIntId
	where PartitionIntId=@PartitionIntId
end
-------------------------------------------------------

-- �� ������� �� � ������������ SKU ------------------------------------------
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ParentObjectIntId
where ObjectId='v DWRSTD Generalized' 
	and ObjectName='������� �� � ������������ SKU' 
	and ProcessTypeIntId=3

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('v DWRSTD Generalized', '������� �� � ������������ SKU', 3)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ParentObjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_PrecisionForecastGeneralized',0)


---- �������
set @PartitionIntId=NULL
select @PartitionIntId=PartitionIntId 
from ProcessObject where ObjectIntId=@ObjectIntId

set @SQLQuery = '<![CDATA[  
SELECT DAddressId, PlanVerId, ABS(Quantity) AS Quantity, QuantityForecast, QuantitySaleOOS, GeneralizedSKUId, TimeId
FROM DW_PrecisionForecastGeneralized
where  TimeId between #MinTimeId# and #MaxTimeId#-1
]]>'

if @PartitionIntId IS NULL begin
	insert into ProcessPartition (Period, SQLQuery, DataSourceIntId) values ('month', @SQLQuery, @DataSourceIntId)

	set @PartitionIntId=@@IDENTITY
	update ProcessObject set PartitionIntId=@PartitionIntId where ObjectIntId=@ObjectIntId
end 
else begin
	update ProcessPartition set SQLQuery=@SQLQuery,
           Period='month',
           DataSourceIntId=@DataSourceIntId
	where PartitionIntId=@PartitionIntId
end
-------------------------------------------------------


-- �� ������� � ������������ SKU ------------------------------------------
set @ObjectIntId=NULL
select @ObjectIntId=o.ObjectIntId from ProcessObject o
	join ProcessTree t on o.ObjectIntId=t.ObjectIntId and t.ParentObjectIntId=@ParentObjectIntId
where ObjectId='v Ware House Saldo Generalized' 
	and ObjectName='������� � ������������ SKU' 
	and ProcessTypeIntId=3

if @ObjectIntId IS NULL begin
	insert into ProcessObject (ObjectId, ObjectName, ProcessTypeIntId) values ('v Ware House Saldo Generalized', '������� � ������������ SKU', 3)
	set @ObjectIntId=@@IDENTITY
	insert into ProcessTree (ParentObjectIntId, ObjectIntId) values (@ParentObjectIntId, @ObjectIntId)
end

delete from ProcessLink where ObjectIntId=@ObjectIntId
insert into ProcessLink (ObjectIntId, TableName, IsActual) values (@ObjectIntId,'DW_CRMWhBalance',0)


---- �������
set @PartitionIntId=NULL
select @PartitionIntId=PartitionIntId 
from ProcessObject where ObjectIntId=@ObjectIntId

set @SQLQuery = '<![CDATA[
SELECT     x.Quantity, x.PrevQuantity, x.CRMWareHouseId, g.GeneralizedSKUId, x.TimeId, x.DAddressId, x.DocumentDate, x.DataPresence
FROM         dbo.DistrStockView AS x INNER JOIN
                      dbo.DWSKUGeneralized AS g ON g.SKUId = x.SKUId
where x.TimeId between #MinTimeId# and #MaxTimeId#-1
]]>'

if @PartitionIntId IS NULL begin
	insert into ProcessPartition (Period, SQLQuery, DataSourceIntId) values ('month', @SQLQuery, @DataSourceIntId)

	set @PartitionIntId=@@IDENTITY
	update ProcessObject set PartitionIntId=@PartitionIntId where ObjectIntId=@ObjectIntId
end 
else begin
	update ProcessPartition set SQLQuery=@SQLQuery,
           Period='month',
           DataSourceIntId=@DataSourceIntId
	where PartitionIntId=@PartitionIntId
end
-------------------------------------------------------
*/